// SPDX-FileCopyrightText: 2019-2020 Jens Lechtenbörger
// SPDX-License-Identifier: CC-BY-SA-4.0

quizInternet = {
    "info": {
        "name":    "", // Should be empty with emacs-reveal
        "main":    "Let's see what you remember about the Internet...",
        "level1":  "Excellent!", // 80-100%
        "level2":  "Well done!", // 60-79%
        "level3":  "You may need to re-read larger parts.", // 40-59%
        "level4":  "Maybe ask for help?",                   // 20-39%
        "level5":  "Please ask for help and restart from the beginning." // 0-19%, no comma here
    },
    "questions": [
	{
            "q": "Select correct statements about the Internet Protocol",
            "a": [
                {"option": "Every device that is part of the Internet implements the Internet Protocol", "correct": true},
                {"option": "128.176.6.250 is a valid address with IPv4", "correct": true},
                {"option": "Some ranges of IP addresses are private in the sense that they can be reused in different networks (not part of presentation)", "correct": true},
                {"option": "IP is a best-effort protocol, which means that the loss of messages is countered with retry mechanisms", "correct": false} // no comma here
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> You may want to revisit earlier slides or ask.</p>" // no comma here
        },
        {
            "q": "Select correct statements about IP addresses",
            "a": [
                {"option": "When forwarding a datagram, the destination IP address changes on a hop-by-hop basis", "correct": false},
                {"option": "When forwarding a datagram, the source IP address (usually) remains unchanged", "correct": true},
                {"option": "ARP aims to obtain IP addresses from human-readable names", "correct": false},
                {"option": "DNS aims to obtain IP addresses from human-readable names", "correct": true},
                {"option": "A single device may have multiple IP addresses", "correct": true} // no comma here
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 3 statements are correct.)</span> You may want to revisit <a href=\"#slide-internet-communication-steps-2\">typical communication steps</a>.</p>" // no comma here
        },
	{
            "q": "Select all correct statements about MAC addresses",
            "a": [
                {"option": "IP datagrams do not contain MAC addresses (usually)", "correct": true},
                {"option": "ARP aims to obtain MAC addresses for known IP addresses", "correct": true},
                {"option": "ARP aims to obtain IP addresses for known MAC addresses", "correct": false},
                {"option": "An ARP request uses a special broadcast address as target MAC address, which means that every device on the Internet receives that request", "correct": false} // no comma here
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 2 statements are correct.)</span> At what layer do MAC addresses occur?</p>" // no comma here
        },
	{
            "q": "Select all correct statements about demux keys",
            "a": [
                {"option": "A demux key is used to encrypt data", "correct": false},
                {"option": "Demux keys are added to headers when going down the protocol stack", "correct": true},
		{"option": "When going down the protocol stack, the demux key indicates the protocol to be used on the next lower layer", "correct": false},
                {"option": "When protocol B encapsulates a message from protocol A, protocol B adds a demux key identifying A", "correct": true} // no comma here
            ],
            "correct": "<p><span>Correct!</span></p>",
            "incorrect": "<p><span>No. (Hint: 2 statements are correct.)</span> Please revisit <a href=\"#slide-message-encapsulation\">this slide</a>, its notes and the subsequent slide.</p>" // no comma here
        } // no comma here
    ]
};
