<!--- Local IspellDict: en -->
<!--- SPDX-FileCopyrightText: 2019-2020 Lechtenbörger -->
<!--- SPDX-License-Identifier: CC-BY-SA-4.0 -->

Org source files in this project are not meant to be exported directly
but to be included in small wrapper files that add title slide, theme,
etc.  See [presentations on Communication and Collaboration Systems](https://gitlab.com/oer/oer-courses/cacs)
for examples.
