# Local IspellDict: en
#+STARTUP: showeverything

#+SPDX-FileCopyrightText: 2018-2023 Jens Lechtenbörger <https://lechten.gitlab.io/#me>
#+SPDX-License-Identifier: CC-BY-SA-4.0

#+KEYWORDS: web, www, HTTP, GET request,

* Web
** History of the Web (1/2)
   - 1945, [[https://www.theatlantic.com/magazine/archive/1945/07/as-we-may-think/303881/][Vannevar Bush: As we may think]]
       - Memex for information storage
       - Associative indexing (Hyperlinks)
   - 1989, [[https://www.w3.org/History/1989/proposal.html][article by Tim Berners-Lee]]
       - Distributed hypertext system, “»web« of notes with links”
       - Initially for cooperation among physicists at CERN
   - May 1991
     - Distributed information system based on HTML, HTTP, and client
       software at CERN
   - August 1991
     - Availability of CERN files [[https://www.w3.org/People/Berners-Lee/1991/08/art-6484.txt][announced]] in [[news:alt.hypertext]]

** History of the Web (2/2)
   - 1992, NCSA *Web Server* available
	 - National Center for Supercomputing Applications, University of
           Illinois, Urbana-Champaigne
   - 1993, Mosaic *browser* created at NCSA
   - 1994, [[https://www.w3.org/][World Wide Web Consortium]] (W3C) founded by Tim Berners-Lee
      - Publication of technical reports and “recommendations”
   - Now
      - Web 2.0, Semantic Web (aka Web 3.0 cite:Hen09), cloud computing, browser as access device

** WWW/Web
   - Standards
     - W3C ([[https://www.w3.org/TR/html4/intro/intro.html][HTML 4 Specification]])
       - “The World Wide Web (Web) is a network of information resources.”
     - [[https://tools.ietf.org/html/rfc7230][HTTP/1.1 Specification (RFC 7230)]]
       - “The Hypertext Transfer Protocol (HTTP) is a stateless application-level
	 protocol for distributed, collaborative, hypertext information
	 systems.”
   - Distributed information system
     - Client-Server architecture
       - Web clients (browsers) and servers exchange HTTP messages based on
         [[basic:https://oer.gitlab.io/oer-courses/cacs/Internet.html][Internet]]
         standards
     - Sample Web standards (application layer of Internet architecture)
       - URIs (Uniform Resource Identifiers, next slide)
       - HTTP (this presentation)
       - ((X)HTML, CSS)

*** URI
    :PROPERTIES:
    :CUSTOM_ID: uri
    :END:
    - URI = Character string to *identify* entities
      - [[https://datatracker.ietf.org/doc/html/rfc3986][RFC 3986]]
        - Format: [[color:darkgreen][<scheme>]]:<scheme-specific-part>
    - Examples from RFC 3986 (some containing [[color:darkred][DNS names]])
      - {{{greencode(http)}}}{{{bluecode(:)}}}{{{blackcode(//)}}}{{{redcode(www.ietf.org)}}}{{{blackcode(/rfc/rfc2396.txt)}}}
      - {{{greencode(ldap)}}}{{{bluecode(:)}}}{{{blackcode(//[2001:db8::7]/c=GB?objectClass?one)}}}
      - {{{greencode(mailto)}}}{{{bluecode(:)}}}{{{blackcode(John.Doe@)}}}{{{redcode(example.com)}}}
      - {{{greencode(tel)}}}{{{bluecode(:)}}}{{{blackcode(+1-816-555-1212)}}}
    - Scheme-specific part often structured
      - [[color:darkgreen][<scheme>]]://<authority><path>?<query>#fragment
      - E.g.,
        https://oer.gitlab.io/oer-courses/cacs/Web-and-E-Mail.html?default-navigation#slide-uri

* HTTP
** HTTP
   - Hypertext Transfer Protocol
     - [[https://tools.ietf.org/html/rfc7230][HTTP/1.1, RFC 7230]]
       - Plain text messages, discussed subsequently
     - [[beyond:https://tools.ietf.org/html/rfc7540][HTTP/2, RFC 7540]]
       - Adds frame format with compression
     - [[beyond:https://en.wikipedia.org/wiki/HTTP/3][HTTP/3]] also [[https://w3techs.com/technologies/details/ce-http3][upcoming]]
       - Uses [[beyond:https://en.wikipedia.org/wiki/QUIC][QUIC]] over
         UDP as transport layer protocol
   - Request/response protocol
     - Specific message format
     - Several access methods
     - Requires [[basic:https://oer.gitlab.io/oer-courses/cacs/Internet.html#slide-ip-udp-tcp][reliable transport protocol]]
       - Traditionally, TCP/IP with port 80 (or port 443 for [[https://en.wikipedia.org/wiki/HTTPS][HTTPS]])

** Excursion: Manual Connections
   :PROPERTIES:
   :CUSTOM_ID: manual-connections-tls
   :END:
   - [[https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol][HTTP]]
     (before HTTP/2) and
     [[https://en.wikipedia.org/wiki/Simple_Mail_Transfer_Protocol][SMTP]]
     are plain text protocols
     - With encrypted variants
       [[https://en.wikipedia.org/wiki/HTTPS][HTTPS]] and
       [[https://en.wikipedia.org/wiki/SMTPS][SMTPS]]
       (or [[https://en.wikipedia.org/wiki/STARTTLS][STARTTLS]])
   - Enables experiments on the command line
     - Type (or copy&paste) request, see server response
     - For unencrypted connections, ~telnet~ can be used (preinstalled
       or available for lots of OSs)
     - For encrypted connections, ~gnutls-cli~ can be used (part of
       [[https://www.gnutls.org/][GnuTLS]], which is
       [[https://oer.gitlab.io/OS/Operating-Systems-Motivation.html#slide-free-software][free software]])
       - TLS =
         [[https://en.wikipedia.org/wiki/Transport_Layer_Security][Transport Layer Security]]
         - Successor to SSL
         - Layer between application layer and TCP, recall
           [[https://oer.gitlab.io/oer-courses/cacs/Internet.html#slide-internet-architecture][Internet architecture]]
         - Secure channels based on [[https://oer.gitlab.io/oer-courses/cacs/Internet.html#slide-drawing-hybrid-enc][asymmetric cryptography]]

*** Warnings
    - Next two slides demonstrate how to type HTTP commands
      (for an improved understanding of the protocol)
      - Subsequent examples with
        ~www.informationelle-selbstbestimmung-im-internet.de~
        *require* GnuTLS
        - Server redirects from port 80 to port 443
      - If your manual typing is too slow, connections may *time out*
        (e.g., “Peer has closed the GnuTLS connection”)
      - Also, use of backspace or cursor keys may destroy connections
    - Suggestion: Type in text editor and copy&paste into command line


*** telnet
    :PROPERTIES:
    :CUSTOM_ID: telnet
    :reveal_extra_attr: data-audio-src="./audio/telnet.ogg"
    :END:
    - Original ~telnet~ purpose: Login to remote host
      - *Insecure* plaintext passwords
      - Nowadays, remote login performed with
        [[https://en.wikipedia.org/wiki/Secure_Shell][Secure Shell]],
        ~ssh~
    - Establish *TCP connection* to destination port
      - ~telnet www.google.de 80~ (port 80 for HTTP)
        - (For variants without visual feedback possibly followed by
          ~ctrl-+~ or ~ctrl-]~, ~set localecho~ [enter] [enter])
        - ~GET / HTTP/1.1~ [enter]
        - ~Host: www.google.de~ [enter] [enter]
        - ([[https://gitlab.com/oer/figures/-/raw/master/DS/http-telnet.gif][Screencast]])
	- (Context for above lines [[#http-request][soon]])
      - *Beware*: Buggy telnet implementations may stop sending after first
        line (use Wireshark to verify)
    #+begin_notes
Here, you see a sample use of ~telnet~ to open a TCP connection to
port 80 on a Google server.  You could try out any other number to
check on what ports the server is prepared to talk with you.
Port 80 is reserved for HTTP, which is slowly phased out in favor of
the cryptographically secured variant HTTPS on port 443.

Anyways, once a TCP connection is established successfully, you can
send data to the server by typing it.  When typing, you need to “speak”
the protocol that is expected by the server, here HTTP, and the lines
starting with ~GET~ as well as with ~Host~ are both part of the
HTTP protocol, which is explained on later slides.

Note that you cannot use ~telnet~ with encrypted connections as you
would need to type bytes that setup and use cryptographic protocols
then.  Thus, while you can /open/ a TCP connection to port 443 with
~telnet~, it is unlikely that you can /use/ that connection by typing
the necessary bytes for cryptographic protocols afterwards.

For cryptographically secured connections, you may want to use the
GnuTLS client as shown on the next slide.

An aside: On the slide, “ctrl-+” means: Press the ~ctrl~ key and
~+~ simultaneously.  Similarly for other keys.
    #+end_notes

*** gnutls-cli
    :PROPERTIES:
    :CUSTOM_ID: gnutls
    :reveal_extra_attr: data-audio-src="./audio/gnutls.ogg"
    :END:
    - Establish [[https://en.wikipedia.org/wiki/Transport_Layer_Security][TLS]]
      protected TCP connection with [[https://www.gnutls.org/][GnuTLS]]
      - Alternative to ~telnet~ on previous slide ([[https://gitlab.com/oer/figures/-/raw/master/DS/http-gnutls-cli.gif][screencast]])
      - ~gnutls-cli --crlf www.informationelle-selbstbestimmung-im-internet.de~
        - (HTTPS on port 443 by default)
        - ~GET /chaosreader.html HTTP/1.1~ [enter]
        - ~Host: www.informationelle-selbstbestimmung-im-internet.de~ [enter] [enter]
      - [[https://oer.gitlab.io/oer-courses/cacs/Web-and-E-Mail.html#slide-smtp][SMTP]]
        for e-mail, port 587 as alternative to 25
        - ~gnutls-cli --crlf --starttls -p 587 secmail.uni-muenster.de~
          - (Type ~ehlo localhost~, then ~starttls~; press ~ctrl-d~ to
            enter TLS mode; needs authentication)
    #+begin_notes
The cryptographic protocol suite TLS is used in two major variants.

1. A special port, e.g., 443, is reserved for cryptographically
   secured connections.  The connecting client (here, GnuTLS)
   must immediately “talk” a cryptographic protocol.
2. A single port, e.g., 25, supports plaintext as well as
   cryptographically secured connections.  Here, the client starts
   with plaintext (as with ~telnet~), but can issue a specific command
   (here, ~starttls~ followed by ~ctrl-d~) to switch to a
   cryptographically secured connection.
   - “ctrl-d” means: Press the ~ctrl~ key and ~d~ simultaneously.

In any case, application data is transmitted through secure channels.
    #+end_notes

** Excursion: Browser Tools
   - Modern browsers offer developer tools
     - E.g., press ~ctrl-shift-I~ with Firefox
     - Tools to inspect HTML, CSS, Javascript
     - Tools to inspect HTTP traffic (Network tab)
       - Live view on browser requests and server responses
         - With details on timing, caching, headers
     - Console with error messages
     - And much more

** HTTP Messages
   :PROPERTIES:
   :CUSTOM_ID: http-request
   :END:
   - Requests and responses
     - Generic message format of
       [[https://tools.ietf.org/html/rfc822][RFC 822]], 1982
       (822→2822→[[https://tools.ietf.org/html/rfc5322][5322]])
       - Originally for e-mail, extensions for binary data
	 - Lines [[https://en.wikipedia.org/wiki/Newline][end with CRLF]],
           ~\r\n~ below (press ~enter~, do not type this)
     - Messages consist of
       - Headers
	 - In HTTP always a distinguished [[color:darkblue][start-line]] (request or status)
	 - Then zero or more [[color:darkgreen][headers]]
       - [[color:darkred][Empty line]]
       - Optional message body
     - Sample ~GET~ *request* (does not have a body)
       - {{{bluecode(GET /chaosreader.html HTTP/1.1\r\n)}}}\\
         {{{greencode(Host: www.informationelle-selbstbestimmung-im-internet.de\r\n)}}}\\
         {{{redcode(\r\n)}}}

#+REVEAL: split
   - Excerpt of sample HTTP *response* to previous ~GET~ request
     - {{{bluecode(HTTP/1.1 200 OK\r\n)}}}\\
       {{{greencode(Date: Wed\, 08 Apr 2020 13:30:10 GMT\r\n)}}}\\
       {{{greencode(Server: Apache\r\n)}}}\\
       {{{greencode(Last-Modified: Wed\, 24 Jul 2019 12:25:46 GMT\r\n)}}}\\
       {{{greencode(ETag: "2cd1-58e6c6898dce2"\r\n)}}}\\
       {{{greencode(Content-Length: 11473\r\n)}}}\\
       {{{greencode(more headers omitted)}}}\\
       {{{greencode(Content-type: text/html; charset=utf-8\r\n)}}}\\
       {{{redcode(\r\n)}}}\\
       {{{blackcode(HTML code as body)}}}

** HTTP Methods
   - Case-sensitive (capital letters)
     - ~GET~ (Request for resource, see [[https://tools.ietf.org/html/rfc7231#section-4.3.1][section 4.3.1]])
     - ~HEAD~ (Request information on resource, see [[https://tools.ietf.org/html/rfc7231#section-4.3.2][section 4.3.2]])
     - ~POST~ (Transfers entity, see [[https://tools.ietf.org/html/rfc7231#section-4.3.3][section 4.3.3]])
       - Annotations, postings, forms, database extensions
     - ~PUT~ (Creates new resource on server, see [[https://tools.ietf.org/html/rfc7231#section-4.3.4][section 4.3.4]])
     - ~DELETE~ (Deletes resource from server, see [[https://tools.ietf.org/html/rfc7231#section-4.3.5][section 4.3.5]])
     - ~CONNECT~ (Establish tunnel with proxy, see [[https://tools.ietf.org/html/rfc7231#section-4.3.6][section 4.3.6]])
     - ~OPTIONS~ (Asks for server capabilities, see [[https://tools.ietf.org/html/rfc7231#section-4.3.7][section 4.3.7]])
     - ~TRACE~ (Tracing of messages through proxies, see [[https://tools.ietf.org/html/rfc7231#section-4.3.8][section 4.3.8]])

** Conditional GET
   :PROPERTIES:
   :CUSTOM_ID: conditional-get
   :reveal_extra_attr: data-audio-src="./audio/etag.ogg"
   :END:
   - ~GET~ under conditions
     - Requires (case-insensitive) request header
       - (Can be used by browser to check if [[https://oer.gitlab.io/oer-courses/cacs/Web-and-E-Mail.html#slide-http-caching][cached]]
         version still fresh)
       - Different types, e.g.: ~If-Modified-Since~, ~If-Match~, ~If-None-Match~
   - Example
     - Request
       - {{{bluecode(GET /chaosreader.html HTTP/1.1)}}}\\
	 {{{greencode(Host: www.informationelle-selbstbestimmung-im-internet.de)}}}\\
	 {{{greencode(If-None-Match: "2cd1-58e6c6898dce2")}}}
     - Response
       - {{{bluecode(HTTP/1.1 304 Not Modified)}}}\\
	 {{{greencode(Date: Wed\, 08 Apr 2020 14:07:31 GMT)}}}\\
	 {{{greencode(additional headers)}}}
   #+begin_notes
Please revisit the response for an [[#http-request][earlier HTTP request]].
Note that the response contains a ~Last-Modified~ date and an ~ETag~.
Both pieces can be used for conditional gets.  While the date is
probably self-explanatory, the ETag is some version identifier
provided by the server.  Changed page contents are reflected in
changed ETag values (but not necessarily the other way round).

On this slide, you see a conditional GET with the ETag value
~"2cd1-58e6c6898dce2"~ from the previous response.  As the server’s
ETag value did not change, it responds with status code 304,
indicating that no modification took place.  Hence, a cached result
would still be fresh and usable, saving bandwidth and reducing
transmission delays.
   #+end_notes
# For copy and paste:
# gnutls-cli --crlf www.informationelle-selbstbestimmung-im-internet.de
# GET /chaosreader.html HTTP/1.1
# Host: www.informationelle-selbstbestimmung-im-internet.de
# If-None-Match: "2cd1-58e6c6898dce2"

** Sample Status Codes
   - Three digits, first one for class of response
     - 1xx: Informational - Request received, continuing process
       - 100: Continue - Client may continue with request body
     - 2xx: Successful - Request successfully received, understood, and
       accepted
       - 200: OK
     - 3xx: Redirection - Further action necessary to complete request
       - 302: Found (temporarily under different URI)
       - 303: See Other (redirect to different URI in ~Location~ header)
       - 304: Not Modified ([[#conditional-get][previous slide]])
     - 4xx: Client Error - Request with bad syntax or cannot be
       fulfilled
       - 403: Forbidden
       - 404: Not Found
     - 5xx: Server Error - Server failed for apparently valid
       request

# Local Variables:
# indent-tabs-mode: nil
# oer-reveal-master: nil
# End:
